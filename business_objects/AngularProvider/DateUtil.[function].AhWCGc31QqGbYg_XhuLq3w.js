function() {
    const Util = {};
    
    const eraTransition = Util.eraTransition = moment('0001-01-01');
    
    const dpy = Util.daysPerYear = 365.2425; //days per year, per gregorian calendar
    
    
    
    //val - multiply by input to get days
    var unitScale = 
    Util.unitScale = {
        billion:dpy*1000000000,
        million:dpy*1000000,
        mill:   dpy*1000,
        cent:   dpy*100,
        decades:dpy*10,
        years:  dpy,
        days:1,
        hours:(1/24),
        minutes:(1/(24*60)),
        seconds:(1/(24*60*60))
    };
    
    const units = 
    Util.durationUnits = [
        { val:'billion', label:'Billion Years'},
        { val:'million', label:'Million Years'},
        { val:'mill', label:'Millennia'},
        { val:'cent', label:'Centuries'},
        { val:'decades', label:'Decades'},
        { val:'years', label:'Years'},
        { val:'days', label:'Days'},
        { val:'hours', label:'Hours'},
        { val:'minutes', label:'Minutes'},
        { val:'seconds', label:'Seconds'},
    ];
    
    
    const oneMillion = 1000000;
    const oneBillion = oneMillion*1000;
    Util.abbreviateLargeNum = function(num, options) {
        const fmtOptions = options || {
            minimumFractionDigits:2,
            maximumFractionDigits:2
        };
        
        var suffix = '';
        if(num >= oneBillion) {
            suffix='B';
            num = num / oneBillion;
        }
        else if(num >= oneMillion) {
            suffix='M';
            num = num / oneMillion;
        }
        
        return num.toLocaleString('en-us', fmtOptions)+suffix;
    };
    
    
    /**
     * determine the unit of time for given # of days that results in fewest digits
     */
    Util.findBestUnit = function(numDays) {
        
        var closestDist = Number.MAX_VALUE;
        var closestUnit, closestVal;
        
        _.forEach(units, u=>{
            var factor = unitScale[u.val];
            
            var myDist = Math.abs(Math.log10(numDays/factor));
            
            if(myDist < closestDist) {
                closestDist = myDist;
                closestUnit = u.label;
                closestVal = numDays/factor;
            }
        });
        
        return {value:closestVal, unit:closestUnit}; 
    };
    
    Util.formatTimespan = function(numDays) {
        if(numDays < 0) {
            numDays = -numDays;
        }
        var bu = Util.findBestUnit(numDays);
        return Util.abbreviateLargeNum(bu.value)+' '+bu.unit;
    }
    
    
    return Util;
}