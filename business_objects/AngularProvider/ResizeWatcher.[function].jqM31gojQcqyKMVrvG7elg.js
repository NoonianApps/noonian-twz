function($window, $timeout) {
    
    const ResizeWatcher = function(callback, debounceMs) {
        debounceMs = debounceMs || 500;
        
        var resizePromise = false;
        
        //Might be called many times rapidly as window is resized
        this.onResize = function(evt) {
            if(resizePromise) {
                $timeout.cancel(resizePromise);
            }
            
            //only when size is stable for debounceMs milliseconds do we notify callback
            resizePromise = $timeout(function() {
                resizePromise = false;
                callback();
            }, debounceMs); 
        };
        
        
    };
    
    ResizeWatcher.prototype.enable = function() {
        $window.addEventListener('resize', this.onResize);
    }
    
    ResizeWatcher.prototype.disable = function() {
        $window.removeEventListener('resize', this.onResize);
    }
    
    
    return ResizeWatcher;
}