function () {
    const moment = require('moment');
    const EPOCH_STR = '1970-01-01';
    
    const epochDate = moment(EPOCH_STR);
    
    const dpy = 365.2425; //days per year, per gregorian calendar
    
    //number of days between era switch (BC->AD) and unix epoch 
    const eraToEpoch = epochDate.diff(moment('0001-01-01'), 'days');
    
    const exports = {};
    
    
    /**
     * Convert a date into "epoch days" number;
     *   that is, the positive number of days prior to unix epoch
     *   (bigger number => further in the past)
     */
    exports.dateToEpochDays = function(dateStrOrObj) {
        if(!dateStrOrObj) {
            return null;
        }
        if(typeof dateStrOrObj === 'string') {
            // epoch date - date
            return epochDate.diff(moment(dateStr), 'days');
        }
        else {
            let d = dateStrOrObj;
            let era = d.era && d.era.toLowerCase();
            let isBc = (era === 'bc' || era === 'bce');
            
            if(!isBc) {
                return epochDate.diff(moment(d), 'days');
            }
            else {
                //For BC, don't worry about month and day
                let preEraDays = d.year*dpy;
                return preEraDays + eraToEpoch;
                
            }
        }
    };
    
    /**
     * Shift a dayCount specified prior to fromDate
     * to a daycount prior to unix epoch
     */
    exports.shiftToEpoch = function(dayCount, fromDate) {
        var diff = moment(fromDate).diff(epochDate, 'days');
        return dayCount - diff;
    };
    
    /**
     *  Difference between unix epoch and specified date
     *  (add this value to a daycount to effectively shift the epoch)
     */
    exports.epochDiff = function(fromDate) {
        return moment(fromDate).diff(epochDate, 'days');
    };
    
    
    //val - multiply by input to get days
    exports.unitScale = {
        billion:dpy*1000000000,
        million:dpy*1000000,
        mill:   dpy*1000,
        cent:   dpy*100,
        decades:dpy*10,
        years:  dpy,
        days:1,
        hours:(1/24),
        minutes:(1/(24*60)),
        seconds:(1/(24*60*60))
    };
    
    
    return exports;
}