function(InterpolationTool) {
    const exports = {};
    
    const UNIT_RULER_FRAC = 10; //fraction of window to use as unit ruler length
    const MAX_S = 20;  //Smallest "ruler" for measuring wave
    
    
    
    //Get the two points on circle <h,k,r> @ x
    const getCircleFn = (h, k, r)=>{
        const rSquared = r*r;
        
        return function(x) {
            let radicalPart = Math.sqrt(rSquared - (x-h)*(x-h));
            return [
                k + radicalPart,
                k - radicalPart
            ];
        }
    };
    
    const dist = (x1, y1, x2, y2)=>{
        return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    };
    
    //find value of x where dist(<h,k>, <x,f(x)>) is close enough to r (w/in tolerance)
    const MAX_STACK=1000;
    const findIntersection = (tolerance, h, k, f, r, left, right, stack)=>{
        stack = stack || 0;
        
        let width = right - left;
        let xInc = width/100;
        
        let bestX, bestVal = Number.MAX_VALUE;
        
        for(let x = left; x <= right; x += xInc) {
            let d = dist(h, k, x, f(x));
            let diff = Math.abs(r - d);
            if(diff < bestVal) {
                bestX = x;
                bestVal = diff;
            }
        }
        
        if(bestVal <= tolerance) {
            return bestX;
        }
        else if(stack > MAX_STACK) {
            console.log('EXCEEDED STACK LIMIT IN findIntersection: ', tolerance, h, k, f, r, left, right);
            //kludge for now until figure out why bestVal might not get close enough to tolerance
            // (may be limit on data type)
            return bestX;
        }
        else {
            return findIntersection(tolerance, h, k, f, r, bestX-xInc, bestX+xInc, stack+1);
        }
        
    };
    
    exports.calcFractalDimension = function(fn, leftBoundary, rightBoundary) {
        let unitLength = Math.abs(rightBoundary - leftBoundary)/UNIT_RULER_FRAC;
        
        const pointList = []; 
        
        const L = [0];  //measured length at each successive scale
        
        const logG = [], logL = []; //graph whose slope determines fractal dimension
        
        for(let s=1; s < MAX_S; s++) {
            var currPointList = pointList[s] = [];
            
            var r = unitLength / s; //r = our current ruler length (Called "G" in MathWithoutBorders vid)
            
            var count = 0;  //Number of rulers measured at this scale
            
            var xIter = leftBoundary; //x used to iterate from left to right
            
            var iterInc = r/100; //iteration increment
            //TODO how to determine good scan resolution? 
            
            while(xIter < rightBoundary) {
                
                //Center a circle @ current position
                let h = xIter, k = fn(xIter);
                let circleFn = getCircleFn(h, k, r);
                
                //Iterate until function exits the circle
                var inside = true; //We know the center is inside!
                do {
                    xIter += iterInc;
                    
                    let y = fn(xIter);
                    let circleBounds = circleFn(xIter);
                    
                    inside = y < circleBounds[0] && y > circleBounds[1];
                    
                } while(inside);
                
                //Now xIter is the first point we found outside the circle
                // lets, scan between it and the last one that was inside to get 
                // a more precise intersection point 
                
                let xIntersect = findIntersection(r/1000, h, k, fn, r, xIter - iterInc, xIter);
                
               
                
                if(xIntersect < rightBoundary) {
                    pointList[s].push({x:xIntersect,y:fn(xIntersect), r});
                    count++;
                }
                else {
                    let partial = dist(h, k, rightBoundary, fn(rightBoundary))/r;
                    count += partial;
                    
                    pointList[s].push({x:rightBoundary,y:fn(rightBoundary), r:partial});
                }
                
                xIter = xIntersect;
                
            }
            
            
            //We've counted up all the rulers of length r..
            
            L.push(count*r);
            
            logG.push(Math.log(s));
            logL.push(Math.log(count*r));
            
        }
        
        //Graph Log(s) vs. Log(L(s))
        //Slope is 1 - D, where D is the fractal dimension
        const bestFitLine = InterpolationTool.bestFitLine(logG, logL);
        const slope = bestFitLine.m;
        const D =  1 - slope; 
        
        return {
            D,
            L, 
            logG,
            logL,
            pointList
        };
        
    };
    
    
    return exports;
}