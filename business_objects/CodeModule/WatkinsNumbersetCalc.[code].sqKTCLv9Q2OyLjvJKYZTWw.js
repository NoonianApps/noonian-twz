function () {
    const exports = {};
    
    //First Order of Difference
    const FOD = [  
        3,
        6,2,4,4,4,3,2,4,2,4,6,2,2,4,2,2,
        6,3,4,3,2,2,2,3,4,2,6,2,6,3,2,3,
        4,4,4,2,4,6,4,3,2,4,2,3,4,3,2,3,
        4,4,4,1,6,2,2,3,4,3,2,1,6,3,6,3
    ];
    
    
    // 
    
    const exp_minus_one = function(i) {
        i = Math.trunc(i);
        
        if ( i < 0 )
            i *= -1;

        return ( i%2 ? -1 : 1 );
    };
    
    const mod_64 = function(i) {
        i = Math.trunc(i);
        while ( i < 0 ) 
            i += 64;
        
        return Math.abs( i%64 );
    };


     
     //Generate the original Kelley numberset sequence using condensed formula derived by Watkins
     
    exports.generateOriginal = function() {
        
        var h = FOD;
        var w = [];
        
        for (let k=0; k<=383; k++ )
        {
            //a = Angular term:
            let a =
                
                (exp_minus_one((k-1)/32)) * 
                    ( 
                        h[mod_64(k-1)] - h[mod_64(k-2)] +   //forward portion
                        h[mod_64(-k)] - h[mod_64(1-k)]      //reverse portion
                    )
                
                +
                
                3 * exp_minus_one((k-3)/96) *
                    (
                        h[mod_64((k/3)-1)] - h[mod_64((k/3)-2)] +   //forward
                        h[mod_64(-1*(k/3))] - h[mod_64(1-(k/3))]    //reverse
                    )
                
                + 
                
                6 * exp_minus_one((k-6)/192) *
                    (
                       h[mod_64((k/6)-1)] - h[mod_64((k/6)-2)] +    //forward
                        h[mod_64(-1*(k/6))] - h[mod_64(1-(k/6))]    //reverse
                    );
            
            
            //b = Linear term:
            let b = 
                ( 9 - h[mod_64(-k)] - h[mod_64(k-1)] )
                +
                3 * ( 9 - h[mod_64(-1*(k/3))] - h[mod_64((k/3)-1)] )
                +  
                6 * ( 9 - h[mod_64(-1*(k/6))] - h[mod_64((k/6)-1)] );
            
            // console.log(a, b);
            w.push(Math.abs(a) + Math.abs(b));
            
        }
        
        return w;
    };
    
    
    exports.generateWatkins = function() {
        
        var h = FOD;
        var w = [];
        
        for (let k=0; k<=383; k++ )
        {
            //a = Angular term:
            let a =
            
                1 *
                    ( 
                        h[mod_64(k-1)] - h[mod_64(k-2)] +   //forward portion
                        h[mod_64(-k)] - h[mod_64(1-k)]      //reverse portion
                    )
                
                +
                
                3 * 
                    (
                        h[mod_64((k/3)-1)] - h[mod_64((k/3)-2)] +   //forward
                        h[mod_64(-1*(k/3))] - h[mod_64(1-(k/3))]    //reverse
                    )
                
                + 
                
                6 *
                    (
                       h[mod_64((k/6)-1)] - h[mod_64((k/6)-2)] +    //forward
                        h[mod_64(-1*(k/6))] - h[mod_64(1-(k/6))]    //reverse
                    );
            
            
            //b = Linear term:
            let b = 
                ( 9 - h[mod_64(-k)] - h[mod_64(k-1)] )
                +
                3 * ( 9 - h[mod_64(-1*(k/3))] - h[mod_64((k/3)-1)] )
                +  
                6 * ( 9 - h[mod_64(-1*(k/6))] - h[mod_64((k/6)-1)] );
            
            // console.log(a, b);
            w.push(Math.abs(a) + Math.abs(b));
            
        }
        
        return w;
    };
    
    return exports;
    
}