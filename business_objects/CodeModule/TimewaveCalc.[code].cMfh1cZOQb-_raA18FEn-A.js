function () {

    const exports = {};

    
    const NUM_POWERS = 64;
    const CALC_PREC = 1000000;
    const NUM_DATA_POINTS = 384;
    
    var numberSets; //Initialized @ bottom 
    
    
    var powerLookupTables = {};
    
    const initPowerLookup = function(waveFactor) {
        if(!powerLookupTables[waveFactor]) {
            var pt = powerLookupTables[waveFactor] = [1];
            for(let i=1; i < NUM_POWERS; i++) {
                pt.push(waveFactor * pt[i-1]);
            }
        }
        return powerLookupTables[waveFactor];
    };
    
    
    /**
     *  v is the continuous piecewise-linear function derived from a 
     *  linear interpolation of the numberset
     */
    const v = function(y, numberSet) {
        var i = Math.floor(y % NUM_DATA_POINTS);
        var j = (i+1) % NUM_DATA_POINTS;
        var z = y - Math.floor(y);
        
        if(z == 0) {
            return numberSet[i];
        }
        
        //interpolate according to the remainder:
        return (numberSet[j] - numberSet[i]) * z + numberSet[i];
        
    };
    
    
    /**
     *  Perform the infinite sum 
     */
    const computeDataPoint = function(x, numberSet, powers, reflect) {
        
        var sum = 0.0;
        var lastSum = 0.0;
        
        if(reflect) {
            reflect = (x < 0);
            x = Math.abs(x);
        }
        
        if(x) {
            var i;
            for(i=0; x >= powers[i]; i++) {
                let vResult = v((x / powers[i]), numberSet);
                sum += (vResult * powers[i]);
                
            }
            
            i=0; 
            
            do {
                if(++i > CALC_PREC + 2) {
                    break;
                }
                lastSum = sum;
                let vResult = v((x*powers[i]), numberSet);
                sum += (vResult / powers[i]);
                
            } while( sum === 0.0 || sum > lastSum );
        }
        
        sum = sum/powers[3]; //scale down the value for more readable y-axis
        
        return reflect ? -sum : sum;
    };
    
    /**
     * numberSetName = kelley, watkins, sheliak, or huangti  (default kelley)
     * dtz = days to zero-point
     * endDtz = end of window (in days relative to zero point)
     * step = steps in which to decrement time (in minutes)
     * wf = wave factor (default 64, range 2-10000)
     * reflectAtZeroPoint = boolean: if true, post-zero-date vaules will be mirror image of pre-zero-date
     */
    exports.computeWindow = function(numberSet, dtz, endDtz, stepMin, wf, reflectAtZeroPoint) {
        
        var dtzp = dtz;
        endDtz = endDtz || 0;
        
        // const numberSet = numberSets[numberSetName || 'kelley'];
        
        const stepDays = stepMin && stepMin/(60*24);
        const waveFactor = wf || 64;
        
        if(!numberSet || !dtzp || !stepDays || typeof stepDays !== 'number' || stepDays <= 0 ) {
            throw new Error('invalid arguments');
        }
        
        const powers = initPowerLookup(waveFactor);
        
        var data = {
            dtz:[],
            vals:[]
        }
        
        while(dtzp >= endDtz) {
            data.dtz.push(dtzp);
            data.vals.push(computeDataPoint(dtzp, numberSet, powers, reflectAtZeroPoint));
            dtzp -= stepDays;
        }
        
        return data;
        
    };
    
    exports.getFunction = function(numberSet, waveFactor, reflect) {
        const powers = initPowerLookup(waveFactor||64);
        return x=>computeDataPoint(x, numberSet, powers, reflect);
    };
    
    return exports;
}