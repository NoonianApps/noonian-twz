function (db, queryParams, _, Q, req) {
    var val = queryParams.val;
    
    var userFilter = {'public':true};
    if(req.user) {
        userFilter = {$or:[
            userFilter,
            {'created_by._id':req.user._id}
        ]};
    }
    
    var queryObj = {$and:[
        userFilter,
        {tags:{$regex:'^'+val}} 
    ]};
    
    
    return db.TimeMarker.find(queryObj, {tags:1}).then(tmList=>{
        const ret = [];
        _.forEach(tmList, tm=>{
            _.forEach(tm.tags, t=>{
                if(t && t.indexOf(val > -1)) {
                    ret.push(t);
                }
            });
        });
        return ret;
    });
    
    
    
}