function(req, auth, _) {
    return auth.getCurrentUser(req).then(u=>{
        return _.pluck(u.roles, '_disp');
    })
}