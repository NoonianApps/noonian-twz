function (db, TimewaveCalc, queryParams, req, FractalDimensionCalc) {
    
    var numberSetName = queryParams.numberSet || 'watkins';
    var dtz = +queryParams.startWindow || 365;
    var endDtz = +queryParams.endWindow || 0;
    var wf = +queryParams.waveFactor || 64;
    var invert = (queryParams.invert === 'true');
    var fullResult = (queryParams.fullResult === 'true');
    
    let minDiff = 0.00002;
    
    
    var numPoints = +queryParams.numPoints || 90000;
    
    var numDays = dtz - endDtz;
    
    if(numDays < minDiff) {
        throw 'window too small';
    }
    
    const queryObj = {
        $or:[
            {key:numberSetName, owner:null},
            {key:numberSetName, 'owner._id':req.user._id}
        ]
    };
    
    return db.NumberSet.findOne(queryObj).then(nsObj=>{
        if(!nsObj || !nsObj.values || nsObj.values.length !== 384) {
            throw 'invalid numberset: '+numberSetName;
        }
        
        // numPoints * stepMin = total span in minutes = numDays*24*60
        // const stepMin = (numDays*24*60) / numPoints;
        // const points =  TimewaveCalc.computeWindow(nsObj.values, dtz, endDtz, stepMin, wf);
        const fn = TimewaveCalc.getFunction(nsObj.values, wf, invert);
        
        return FractalDimensionCalc.calcFractalDimension(fn, endDtz, dtz);
    }).then(result=>{
        if(fullResult) {
            return result;
        }
        else {
            return result.D;
        }
    });
    
    // return TimewaveCalc.computeWindow(numberSetName, dtz, endDtz, stepMin, wf);
}